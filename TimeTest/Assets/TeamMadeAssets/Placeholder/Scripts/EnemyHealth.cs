﻿using UnityEngine;
using System.Collections;

public class EnemyHealth : MonoBehaviour {
	
	public float health = 100;
	public float damLifeSpawn = 0.1f;
	public Material damColor;
	Material orgColor;
	float timer = 0;
	bool damaged = false;
	MeshRenderer myMesh;

	public void DamageMe (float dm) {
		health -= dm;
		timer = damLifeSpawn;
		if (!damaged) {
			damaged = true;
			myMesh.material = damColor;
		}
		if (health <= 0) {
			Destroy(gameObject);
		}
	}

	// Use this for initialization
	void Start () {
		myMesh = gameObject.GetComponent<MeshRenderer> ();
		orgColor = myMesh.material;
	}
	
	// Update is called once per frame
	void Update () {
		if (timer <= 0) {
			if (damaged) {
				damaged = false;
				myMesh.material = orgColor;
			}
		}
		timer -= Time.deltaTime;
	}
}
