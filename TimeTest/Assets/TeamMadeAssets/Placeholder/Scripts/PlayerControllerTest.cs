﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerControllerTest : MonoBehaviour {

	public float speed = 20;
	public float spa = 2;
	
	float timer = 0;

	public GameObject visualObject;
	public GameObject explosion;

	Vector3 direction;
	Vector3? instPos;

	Rigidbody RB;
	Animator AC;
	
	List<GameObject> enemies = new List<GameObject>();

	public RectTransform rectwtf;
	public RectTransform rectwtfAgain;

	Vector3 wtfVelo = Vector3.zero;
	bool wtfRet = false;

	void OnTriggerEnter (Collider c) {
		enemies.Add (c.transform.parent.gameObject);
	}

	void OnTriggerExit (Collider c) {
		enemies.Remove (c.transform.parent.gameObject);
	}

	// Use this for initialization
	void Start () {
		RB = gameObject.transform.parent.GetComponent<Rigidbody> ();
		AC = gameObject.transform.parent.GetComponentInChildren<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
		direction = InputScript.DirectionTI ();
		instPos = InputScript.CastPointTI (11);
		#if UNITY_EDITOR
		if(Input.GetKeyDown(KeyCode.Mouse0)) {
			if(RectTransformUtility.RectangleContainsScreenPoint(rectwtf, Input.mousePosition, Camera.current) && Time.timeScale == 1) {
				Time.timeScale = 0;
				wtfVelo = 50f * Vector3.down;
			} else if(RectTransformUtility.RectangleContainsScreenPoint(rectwtf, Input.mousePosition, Camera.current) && Time.timeScale == 0 && rectwtf.position.y == rectwtfAgain.position.y) {
				wtfRet = true;
				wtfVelo = 50f * Vector3.up;
			}
		}
		if(Time.timeScale != 0) {
			direction = InputScript.DirectionEI ();
			instPos = InputScript.CastPointEI (11);
		}
		#endif

		if (rectwtf.position.y > rectwtfAgain.position.y && wtfVelo.y < 50) {
			wtfVelo += Time.unscaledDeltaTime * 50f * Vector3.up;
		} else if (rectwtf.position.y < rectwtfAgain.position.y && wtfVelo.y > 0 && !wtfRet) {
			wtfVelo = Vector3.zero;
			rectwtfAgain.position += (rectwtf.position.y - rectwtfAgain.position.y) * Vector3.up;
		} else if (wtfRet && rectwtfAgain.position.y > (rectwtf.position.y + 100)) {
			rectwtfAgain.position += ((rectwtf.position.y + 100) - rectwtfAgain.position.y) * Vector3.up;
			wtfRet = false;
			wtfVelo = Vector3.zero;
			Time.timeScale = 1;
		}
		rectwtfAgain.position += wtfVelo * Time.unscaledDeltaTime;

		if (Time.timeScale != 0) {
			if (instPos != null) {
				GameObject wtf = GameObject.Instantiate (explosion);
				wtf.transform.position = instPos.Value;
			}

			if (direction != Vector3.zero) {
				AC.SetBool ("RunBool", true);
				visualObject.transform.rotation = Quaternion.LookRotation (direction, Vector3.up);
			} else {
				AC.SetBool ("RunBool", false);
			}
			if (enemies.Count > 0) {
				GameObject closest = null;
				for (int i = enemies.Count - 1; i >= 0; i --) {
					if (!enemies [i]) {
						enemies.RemoveAt (i);
					} else {
						if (!closest) {
							closest = enemies [i];
						} else if ((closest.transform.position - transform.position).magnitude > (enemies [i].transform.position - transform.position).magnitude) {
							closest = enemies [i];
						}
					}
				}
				if (closest) {
					visualObject.transform.rotation = Quaternion.LookRotation (new Vector3 ((closest.transform.position - transform.position).x, 0, (closest.transform.position - transform.position).z), Vector3.up);
					if (timer <= 0) {
						timer = spa;
						AC.SetTrigger ("AttackTrigger");
					}
				}
			}
			timer -= Time.deltaTime;
		}
	}

	void FixedUpdate () {
		if (direction == Vector3.zero) {
			RB.position -= (RB.velocity.x * Time.fixedDeltaTime * Vector3.right) + (RB.velocity.z * Time.fixedDeltaTime * Vector3.forward);
		}
		RB.velocity = (speed * direction) + (RB.velocity.y * Vector3.up);
//		if (direction != Vector2.zero) {
//			RB.MoveRotation(Quaternion.LookRotation(new Vector3 (direction.x, 0, direction.y), Vector3.up));
//		}
	}
}
