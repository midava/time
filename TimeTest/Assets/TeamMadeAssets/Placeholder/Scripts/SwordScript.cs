﻿using UnityEngine;
using System.Collections;

public class SwordScript : MonoBehaviour {

	public float damage = 25;

	void OnTriggerEnter (Collider c) {
		c.transform.parent.gameObject.GetComponent<EnemyHealth> ().DamageMe (damage);
	}

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
