﻿using UnityEngine;
using System.Collections;

public class KHBLOOM : MonoBehaviour {

	public float lifeSpawn = 1.5f;
	public float damage;

	public void SetDamage(float dam) {
		damage = dam;
	}
	
	void OnTriggerEnter (Collider c) {
        StoneEyeSwitch SES = c.GetComponent<StoneEyeSwitch>();
        StoneEyeSwitch1 SES1 = c.GetComponent<StoneEyeSwitch1>();
        if (SES) {
            SES.TurnSwich();
        }
        if (SES1)
        {
            SES1.TurnSwich();
        }
        Health health = c.transform.gameObject.GetComponent<Health> ();
        if(health && c.transform.gameObject.GetComponent<CharacterMover>().myCharacterType != CharacterMover.CharacterType.Player) {
            health.Damage(damage);
        }
	}

	// Use this for initialization
	void Start () {
		Destroy (gameObject, lifeSpawn);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
