﻿using UnityEngine;
using System.Collections;

public class TimeController : MonoBehaviour {

	public static bool summer = true;
	public RectTransform rect;
	public GameObject summerRoot;
	public GameObject winterRoot;

	// Use this for initialization
	void Start () {
		winterRoot.SetActive (!summer);
		summerRoot.SetActive (summer);
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Mouse0)) {
			if(RectTransformUtility.RectangleContainsScreenPoint(rect, Input.mousePosition, Camera.current)) {
				summer = !summer;
				if(summerRoot.activeSelf != summer) {
					summerRoot.SetActive (summer);
				}
				if(winterRoot.activeSelf == summer) {
					winterRoot.SetActive (!summer);
				}
			}
		}
	}
}
