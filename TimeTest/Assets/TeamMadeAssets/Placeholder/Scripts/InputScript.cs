﻿using UnityEngine;
using System.Collections;

public class InputScript : MonoBehaviour {

	static float deadZoneSize = 0;
	static float maxZoneSize = 200;

	static Vector2 touchPosition;
	static bool isTouching = false;

	static Vector2 direction;

	static public Vector3 DirectionTI () {
		if (Input.touchCount > 0) {
			if(!isTouching) {
				isTouching = true;
				touchPosition = Input.touches[0].position;
			}
			direction = Input.touches[0].position - touchPosition;
			if(direction.magnitude < deadZoneSize) {
				direction = Vector2.zero;
			} else if (direction.magnitude > maxZoneSize) {
				direction.Normalize();
			} else {
				direction = (direction.magnitude / maxZoneSize) * direction.normalized;
			}
			return new Vector3(direction.x, 0, direction.y);
		} else {
			isTouching = false;
			return Vector3.zero;
		}
	}

	static public Vector3 DirectionEI () {
		direction = Vector2.zero;
		if(Input.GetKey(KeyCode.W)) {
			direction += Vector2.up;
		}
		if(Input.GetKey(KeyCode.S)) {
			direction -= Vector2.up;
		}
		if(Input.GetKey(KeyCode.A)) {
			direction -= Vector2.right;
		}
		if(Input.GetKey(KeyCode.D)) {
			direction += Vector2.right;
		}
		if (direction == Vector2.zero) {
			return Vector3.zero;
		} else {
			return new Vector3(direction.x, 0, direction.y).normalized;
		}
	}

	static public Vector3? CastPointTI (int i) {
		if (Input.touchCount > 1 && Input.touches[1].phase == TouchPhase.Began) {
			Ray camRay = Camera.main.ScreenPointToRay (Input.touches[1].position);
			RaycastHit hitPoint;
			if (Physics.Raycast (camRay, out hitPoint, Mathf.Infinity, 1 << i)) {
				return hitPoint.point;
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	static public Vector3? CastPointEI (int i) {
		if (Input.GetKeyDown (KeyCode.Mouse0)) {
			Ray camRay = Camera.main.ScreenPointToRay (Input.mousePosition);
			RaycastHit hitPoint;
			if (Physics.Raycast (camRay, out hitPoint, Mathf.Infinity, 1 << i)) {
				return hitPoint.point;
			} else {
				return null;
			}
		} else {
			return null;
		}
	}
}
