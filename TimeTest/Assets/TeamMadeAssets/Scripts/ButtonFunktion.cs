﻿using UnityEngine;
using System.Collections;

public class ButtonFunktion : MonoBehaviour {

	public static RectTransform[] rects;
	public static ButtonFunktion[] buttons;

	static int indexCounter = 0;

	public enum MyFunktion {toggleMenu, slowMotion, timeStop, thisisnull};

	public MyFunktion myFunktion = MyFunktion.toggleMenu;

	public void ButtonsFunktion () {
		if (myFunktion == MyFunktion.toggleMenu) {
			UIManager.ToggleMenu();
		} else if (myFunktion == MyFunktion.slowMotion) {
			if (TimeManager.timeScale != 1) {
				TimeManager.timeScale = 1;
			} else {
				TimeManager.timeScale = 0.25f;
			}
			UIManager.CloseMenu();
		} else if (myFunktion == MyFunktion.timeStop) {
			TimeManager.ChangeSeason();
			UIManager.CloseMenu();
		} else {
			UIManager.CloseMenu();
		}
	}

	void Awake () {
        if(rects == null) {
            indexCounter++;
        } else {
            indexCounter = 1;
            rects = null;
        }
	}

	// Use this for initialization
	void Start () {
		if (rects == null) {
			rects = new RectTransform[indexCounter];
			buttons = new ButtonFunktion[indexCounter];
			indexCounter = 0;
		}
		rects [indexCounter] = gameObject.GetComponent<RectTransform> ();
		buttons [indexCounter] = this;
		indexCounter ++;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
