﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AI : AIBase {

	public float idleChance = 10;

	CharacterMover CM;
	Vector3 anchor;

	Vector3 direction;
	Vector3?[] inputs = new Vector3?[2];

	float timer = 0;
	float timerLimit = 0;
	float randomNumber;

    public override void GiveInputs () {
		timer += (1 / CM.GiveTimeScale()) * Time.deltaTime;
		if (timer > timerLimit) {
			timer -= timerLimit;
			timerLimit = Random.Range(0f, 5f);
			if ((anchor - transform.position).magnitude > 10) {
				direction = anchor - transform.position;
				direction -= Vector3.up * direction.y;
				inputs[0] = direction.normalized;
				CM.SetInputs (inputs);
			} else {
				randomNumber = Random.Range(0f, 100f);
				if(randomNumber < idleChance) {
					inputs[0] = Vector3.zero;
					CM.SetInputs (inputs);
				} else {
					inputs[0] = new Vector3 (Random.Range(-1f, 1f), 0, Random.Range(-1f, 1f)).normalized;
					CM.SetInputs (inputs);
				}
			}
		}
	}

	void Awake () {
		CM = gameObject.GetComponent<CharacterMover> ();
		AIList.Add (this);
	}

	// Use this for initialization
	void Start () {
		CM = gameObject.GetComponent<CharacterMover> ();
		anchor = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
