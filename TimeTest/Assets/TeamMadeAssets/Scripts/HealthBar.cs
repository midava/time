﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour {

    public Health health;
    Image healthBar;

    void Awake() {
        healthBar = gameObject.GetComponent<Image>();
    }

    // Use this for initialization
    void Start () {
	    if(!health) {
            for (int i = 0; i < CharacterMover.cMList.Count; i++) {
                if (CharacterMover.cMList[i].myCharacterType == CharacterMover.CharacterType.Player) {
                    health = CharacterMover.cMList[i].gameObject.GetComponent<Health>();
                }
            }
        }
    }
	
	// Update is called once per frame
	void Update () {
        healthBar.fillAmount = health.GetHealth() / health.maxHealth;
	}
}
