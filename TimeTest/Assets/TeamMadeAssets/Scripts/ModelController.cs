﻿using UnityEngine;
using System.Collections;

public class ModelController : MonoBehaviour {

    public Material normalMaterial;
    public Material damagedMaterial;

    [HideInInspector]
    public bool damaged = false;

    CharacterMover CM;
    SkinnedMeshRenderer SMR;

    public void RaiseAttackIndex() {
        CM.RaiseAttackIndex();
    }

    public void ResetAttackIndex() {
        CM.ResetAttackIndex();
    }

    public void NullAttackIndex() {
        CM.NullAttackIndex();
    }

    public void SwitchMaterial () {
        damaged = !damaged;
        if(damaged) {
            Destroy(SMR.material);
            SMR.material = damagedMaterial;
        } else {
            Destroy(SMR.material);
            SMR.material = normalMaterial;
        }
    }

    void Awake() {
        CM = gameObject.GetComponentInParent<CharacterMover>();
        SMR = gameObject.GetComponentInChildren<SkinnedMeshRenderer>();
    }

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void AdvanceAnim() {
        CM.AdvanceAnim();
    }
}
