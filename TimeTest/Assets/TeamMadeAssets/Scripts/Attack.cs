﻿using UnityEngine;
using System.Collections;

public class Attack : MonoBehaviour {

    public bool isInputLoop = false;
	public float damage = 1;
    public float inputInterrupt = 2;
    public bool unDamageInterruptable = false;
    public bool unDamageable = false;
    public bool mustMoveFaceDirection = false;
    public bool speedOverride = false;
    public bool speedLimitIsStatic = false;
    public float speedLimit = 1;
    public float turnSpeed = -1;
    public float slowMotion = 1;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
