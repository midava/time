﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharacterMover : MonoBehaviour, UpdateSyncronizer {

    public enum CharacterType {Enemy, Player, Ally}

	public static List<CharacterMover> cMList = new List<CharacterMover> ();

    public CharacterType myCharacterType = CharacterType.Enemy;

	public float speed = 20;
    public float damagedStateLength = 0.5f;

	public bool timeAffected = true;

    public bool unDamageInterruptable = false;
    public bool unDamageable = false;
    public bool chainDamageInterruptable = false;
    public bool chainDamageable = false;

    public Weapon weapon;
    public GameObject explosion;
	
	Vector3? direction = null;
	Vector3? instPos = null;
	Vector3 lookDirection = Vector3.zero;
	bool attack = false;

    bool death = false;

    float damaged = 0;

	int attackIndex = 0;

	float timeScale = 1;

    bool special = false;
    float special2 = -1;

    Vector3 downwardVelocity = Vector3.zero;

	Animator AC;
	CharacterController CC;
    ModelController MC;

    public GameObject summerModel;
    public GameObject winterModel;

    public float winterSpeedMultiplier = 0.5f;
    public float winterManaMultiplier = 2;

    public void RaiseAttackIndex () {
        if (special2 < -0.5f) {
            attackIndex += 1;
            if(weapon.combo[attackIndex].isInputLoop) {
                special = false;
                special2 = weapon.combo[attackIndex].damage;
            } else {
                special = false;
                special2 = -1;
            }
        }
    }

    public void ResetAttackIndex() {
        attackIndex = 0;
        special = false;
        special2 = -1;
    }

    public void NullAttackIndex() {
        attackIndex = -1;
        special = false;
        special2 = -1;
    }

    public bool UnDamagable () {
        if ((damaged > 0 && chainDamageable == false) || (attackIndex < 0 && unDamageable) || (weapon && attackIndex >= 0 && weapon.combo[attackIndex].unDamageable))
            return true;
        return false;
    }

    public void SetDamaged () {
        damaged = damagedStateLength;
    }

    public float Damage () {
        if (attackIndex >= 0) {
            return weapon.combo[attackIndex].damage;
        } else {
            return 0;
        }
    }

	public void SetInputs (Vector3?[] i) {
        direction = i[0];
        instPos = i [1];
	}

	public void SetInputs (Vector3 v, bool b) {
		lookDirection = v;
		attack = b;
	}

	public void SetInputs (Vector3?[] i, bool b) {
        direction = i[0];
		instPos = i [1];
        if (i[2] == null) {
            lookDirection = Vector3.zero;
        } else {
            lookDirection = i[2].Value;
        }
		attack = b;
    }

    public float GiveTimeScale() {
        return timeScale;
    }

    public void SetTimeScale () {
		if (!timeAffected) {
			if(timeScale != TimeManager.timeScale) {
				timeScale = TimeManager.timeScale;
				AC.speed = (1 / timeScale);
			}
		} else if(timeScale != 1) {
			timeScale = 1;
			AC.speed = 1;
		}
	}

    public void Die () {
        if (!death) {
            death = true;
            cMList.Remove(this);
            AC.SetBool("Death", true);
            CC.enabled = false;
            damaged = 0;
            if (powerDrop) {
                GameObject myDrop = GameObject.Instantiate(powerDrop);
                myDrop.transform.position = transform.position;
            }
            Destroy(gameObject, 10);
        }
    }

    public bool IsDead() {
        return death;
    }

    public void Resurect() {
        if (death) {
            death = false;
            cMList.Add(this);
            AC.SetBool("Death", false);
            CC.enabled = true;
            damaged = 0;
            gameObject.GetComponent<Health>().Heal(gameObject.GetComponent<Health>().maxHealth);
        }
    }

    public void MoveCharacter () {
        //Reset attacking
        if (AC.GetBool ("Attack")) {
            AC.SetBool("Attack", false);
            if (AC.GetBool("Special")) {
                AC.SetBool("Special", false);
            }
        }
        if(AC.GetBool("RunAttack")) {
            AC.SetBool("RunAttack", false);
        }

        if(CC.isGrounded || downwardVelocity.magnitude < 1) {
            AC.SetBool("Falling", false);
        } else {
            AC.SetBool("Falling", true);
        }

        //Movement animations
        if(direction == null) {
            AC.SetFloat("Blend", 0);
        } else {
            AC.SetFloat("Blend", direction.Value.magnitude);
        }
      /*  if (direction != Vector3.zero && direction != null) {
			if (!AC.GetBool ("Moving")) {
				AC.SetBool ("Moving", true);
			}
		} else {
			if (AC.GetBool ("Moving")) {
				AC.SetBool ("Moving", false);
			}
		}*/

        //Attack
        if (attack) {
            if (weapon) {
                if (attackIndex > -2 &&
                    (attackIndex < 0 ||
                    direction == null ||
                    direction.Value.magnitude < weapon.combo[attackIndex].inputInterrupt)) {
                    if (attackIndex >= 0 && weapon.combo[attackIndex].isInputLoop) {
                        if (special && direction == null) {
                            AC.SetBool("Special", true);
                            AC.SetBool("Attack", true);
                            special = false;
                            special2 = -1;
                        } else {
                            if (special2 < 0) {
                                special = false;
                                special2 = -1;
                            } else {
                                if (direction != null) {
                                    special = true;
                                }
                                AC.SetBool("Attack", true);
                                special2 -= Time.deltaTime * (1 / TimeManager.timeScale);
                            }
                        }
                    } else {
                        if (attackIndex < 0) {
                            advanceAnim = false;
                            if (weapon.combo[0].speedLimit > 0.5f) {
                                AC.SetBool("RunAttack", true);
                            } else {
                                AC.SetBool("Attack", true);
                            }
                        } else if(attackIndex + 1 == weapon.combo.Length) {
                            advanceAnim = false;
                            if (weapon.combo[attackIndex].speedLimit > 0.5f) {
                                AC.SetBool("RunAttack", true);
                            } else {
                                AC.SetBool("Attack", true);
                            }
                        } else if ((weapon.combo[attackIndex].speedLimit > 0.5f) == (weapon.combo[attackIndex + 1].speedLimit > 0.5f)) {
                            advanceAnim = false;
                            if (weapon.combo[attackIndex].speedLimit > 0.5f) {
                                AC.SetBool("RunAttack", true);
                            } else {
                                AC.SetBool("Attack", true);
                            }
                        } else if (advanceAnim) {
                            if (weapon.combo[attackIndex + 1].speedLimit > 0.5f) {
                                AC.SetBool("RunAttack", true);
                            } else {
                                AC.SetBool("Attack", true);
                            }
                        }
                    }
                } else {
                    attackIndex = -2;
                }
            }
            attack = false;
        }

        //Special
        if (instPos != null && mana > 0) {
			Ray camRay = Camera.main.ScreenPointToRay (instPos.Value);
			RaycastHit hitPoint;
			if (Physics.Raycast (camRay, out hitPoint, Mathf.Infinity, 1 << LayerMask.NameToLayer("Terrain"))) {
				GameObject wtf = GameObject.Instantiate (explosion);
				wtf.transform.position = hitPoint.point;
                mana -= specialCost;
                if (mana < 0)
                    mana = 0;
			}
		}

        //Turn
        if (!weapon || attackIndex < 0 || weapon.combo[attackIndex].turnSpeed < 0) {
            if (lookDirection != Vector3.zero) {
                AC.transform.rotation = Quaternion.LookRotation(lookDirection, Vector3.up);
            } else if (direction != Vector3.zero && direction != null) {
                AC.transform.rotation = Quaternion.LookRotation(direction.Value, Vector3.up);
            }
        } else {
            if (lookDirection != Vector3.zero) {
                lookDirection = Vector3.RotateTowards(AC.transform.forward.normalized, lookDirection, Mathf.Deg2Rad * weapon.combo[attackIndex].turnSpeed * (1 / timeScale) * Time.deltaTime, 0);
                AC.transform.rotation = Quaternion.LookRotation(lookDirection, Vector3.up);
            } else if (direction != Vector3.zero && direction != null) {
                lookDirection = Vector3.RotateTowards(AC.transform.forward.normalized, direction.Value, Mathf.Deg2Rad * weapon.combo[attackIndex].turnSpeed * (1 / timeScale) * Time.deltaTime, 0);
                AC.transform.rotation = Quaternion.LookRotation(lookDirection, Vector3.up);
            }
        }

        // Move
        if(CC.isGrounded) {
            downwardVelocity = Vector3.zero;
        } else {
            downwardVelocity += (1 / timeScale) * Physics.gravity * Time.deltaTime;
        }

        Vector3 fix = Vector3.zero;

        if (attackIndex < 0 || weapon == null) {
            if (direction == null) {
                //    CC.SimpleMove((1 / timeScale) * speed * Vector3.zero);
                if (!CC.isGrounded)
                    fix = (speed * Vector3.zero).magnitude * Vector3.down;
                CC.Move((1 / timeScale) * (speed * Vector3.zero + downwardVelocity + fix) * Time.deltaTime);
            } else {
                //    CC.SimpleMove((1 / timeScale) * speed * direction.Value);
                if (!CC.isGrounded)
                    fix = (speed * direction.Value).magnitude * Vector3.down;
                CC.Move((1 / timeScale) * (speed * direction.Value + downwardVelocity + fix) * Time.deltaTime);
            }
        } else {
            if (weapon.combo[attackIndex].speedOverride) {
                if(direction == Vector3.zero || direction == null) {
                    direction = AC.transform.forward.normalized;
                } else {
                    direction.Value.Normalize();
                }
            }
            if (direction == null) {
                //    CC.SimpleMove((1 / timeScale) * speed * Vector3.zero);
                if (!CC.isGrounded)
                    fix = (speed * Vector3.zero).magnitude * Vector3.down;
                CC.Move((1 / timeScale) * (speed * Vector3.zero + downwardVelocity + fix) * Time.deltaTime);
            } else {
                if (weapon.combo[attackIndex].mustMoveFaceDirection) {
                    direction = direction.Value.magnitude * AC.transform.forward.normalized;
                }
                if (weapon.combo[attackIndex].speedLimitIsStatic) {
                    //    CC.SimpleMove((1 / timeScale) * weapon.combo[attackIndex].speedLimit * direction.Value);
                    if (!CC.isGrounded)
                        fix = (weapon.combo[attackIndex].speedLimit * direction.Value).magnitude * Vector3.down;
                    CC.Move((1 / timeScale) * (weapon.combo[attackIndex].speedLimit * direction.Value + downwardVelocity + fix) * Time.deltaTime);
                } else {
                    //    CC.SimpleMove((1 / timeScale) * speed * weapon.combo[attackIndex].speedLimit * direction.Value);
                    if (!CC.isGrounded)
                        fix = (speed * weapon.combo[attackIndex].speedLimit * direction.Value).magnitude * Vector3.down;
                    CC.Move((1 / timeScale) * (speed * weapon.combo[attackIndex].speedLimit * direction.Value + downwardVelocity + fix) * Time.deltaTime);
                }
            }
        }

        // Does This Belong Here?
        if (damaged > 0) {
            if(!MC.damaged) {
                MC.SwitchMaterial();
            }
            damaged -= (1 / timeScale) * Time.deltaTime;
        } else if (MC.damaged) {
            damaged = 0;
            MC.SwitchMaterial();
        }
    }

    public void EarlySyncedUpdate() {
        SetTimeScale();
        if (damaged > 0) {
            if (!MC.damaged) {
                MC.SwitchMaterial();
            }
            damaged -= (1 / timeScale) * Time.deltaTime;
        } else if (MC.damaged) {
            damaged = 0;
            MC.SwitchMaterial();
        }
    }

    public void SyncedUpdate() {
        MoveCharacter();
    }

    void Awake () {
		AC = gameObject.GetComponentInChildren<Animator> ();
		CC = gameObject.GetComponent<CharacterController> ();
        MC = gameObject.GetComponentInChildren<ModelController>();
        cMList.Add (this);
        mana = maxMana;
	}

	// Use this for initialization
	void Start () {
        AttackIndexController[] aic = AC.GetBehaviours<AttackIndexController>();
        for(int i = 0; i < aic.Length; i ++) {
            aic[i].CM = this;
        }
        if (summerModel) {
            aic = summerModel.GetComponent<Animator>().GetBehaviours<AttackIndexController>();
            for (int i = 0; i < aic.Length; i++) {
                aic[i].CM = this;
            }
            if(!TimeManager.summer) {
                summerModel.SetActive(false);
            } else {
                summerModel.SetActive(true);
                AC = summerModel.GetComponent<Animator>();
                MC = summerModel.GetComponent<ModelController>();
            }
        }
        if (winterModel) {
            aic = winterModel.GetComponent<Animator>().GetBehaviours<AttackIndexController>();
            for (int i = 0; i < aic.Length; i++) {
                aic[i].CM = this;
            }
            if (TimeManager.summer) {
                winterModel.SetActive(false);
            } else {
                winterModel.SetActive(true);
                AC = winterModel.GetComponent<Animator>();
                MC = winterModel.GetComponent<ModelController>();
                speed *= winterSpeedMultiplier;
                mana *= winterManaMultiplier;
                maxMana *= winterManaMultiplier;
            }
        }
    }
	
	// Update is called once per frame
	void Update () {

	}

    public void ChangePersona() {
        if (summerModel && winterModel) {
            if(TimeManager.summer) {
                winterModel.SetActive(false);
                summerModel.SetActive(true);
                AC = summerModel.GetComponent<Animator>();
                MC = summerModel.GetComponent<ModelController>();
                speed /= winterSpeedMultiplier;
                mana /= winterManaMultiplier;
                maxMana /= winterManaMultiplier;
            } else {
                summerModel.SetActive(false);
                winterModel.SetActive(true);
                AC = winterModel.GetComponent<Animator>();
                MC = winterModel.GetComponent<ModelController>();
                speed *= winterSpeedMultiplier;
                mana *= winterManaMultiplier;
                maxMana *= winterManaMultiplier;
            }
            AttackIndexController[] aic = AC.GetBehaviours<AttackIndexController>();
            NullAttackIndex();
            for (int i = 0; i < aic.Length; i++) {
                aic[i].CM = this;
            }
        }
    }

    public GameObject powerDrop;

    public float maxMana = 100;
    public float mana;
    public float specialCost = 20;

    public float GetMana() {
        return mana;
    }

    public void AddMana (float add) {
        mana += add;
        if (mana > maxMana)
            mana = maxMana;
    }

    bool advanceAnim = false;

    public void AdvanceAnim() {
        advanceAnim = true;
    }
}
