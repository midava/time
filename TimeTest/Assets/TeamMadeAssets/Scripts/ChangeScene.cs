﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ChangeScene : MonoBehaviour {

    public string levelName;
    float timeLeft = 0.6f;


    void OnTriggerEnter(Collider c) {
        if (c.GetComponent<CharacterMover>().myCharacterType == CharacterMover.CharacterType.Player) {
                CharacterMover.cMList = new List<CharacterMover>();
                AIBase.AIList = new List<UpdateSyncronizer>();
                SeasonSupervisor.SSList = new List<SeasonSupervisor>();
                Application.LoadLevel(levelName);
            //}
        }
    }

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
