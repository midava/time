﻿using UnityEngine;
using System.Collections;

public class InputManager : MonoBehaviour {

	public float publicDeadZoneSize = 0;
	public float publicMaxZoneSize = 200;
	
	public CharacterMover publicCM;
	
	#if !UNITY_EDITOR
	static float deadZoneSize = 0;
	static float maxZoneSize = 200;

	static Vector3 touchPosition;
	static bool isTouching = false;
	#endif

	static Vector3?[] inputs = new Vector3?[2];
	static ButtonFunktion bFinput = null;
	
	static Vector3 direction;
	
	static CharacterMover cM;
	
	#if !UNITY_EDITOR
	static void GetTouchPoints() {
		if (Input.touchCount > 0) {
			direction = new Vector3(Input.touches[0].position.x, Input.touches[0].position.y, 0);
			if(!isTouching) {
				isTouching = true;
				touchPosition = direction;
			}
			inputs[0] = direction;
		} else {
			isTouching = false;
			inputs[0] = null;
		}
		if (Input.touchCount > 1 && Input.touches[1].phase == TouchPhase.Began) {
			direction = new Vector3(Input.touches[1].position.x, Input.touches[1].position.y, 0);
			inputs[1] = direction;
		} else {
			inputs[1] = null;
		}
	}
	#endif

	#if UNITY_EDITOR
	static void GetMousePoint() {
		inputs[0] = null;
		if (Input.GetKeyDown (KeyCode.Mouse0)) {
			inputs[1] = Input.mousePosition;
		} else {
			inputs[1] = null;
		}
	}
#endif

    static void Direction() {
#if UNITY_EDITOR
        direction = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical"));
        //direction = Mathf.Max(Mathf.Abs(Input.GetAxis("Horizontal")), Mathf.Abs(Input.GetAxis("Vertical"))) * new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical")).normalized;// Vector3.zero;
        /*	if(Input.GetKey(KeyCode.W)) {
                direction += Vector3.forward;
            }
            if(Input.GetKey(KeyCode.S)) {
                direction -= Vector3.forward;
            }
            if(Input.GetKey(KeyCode.A)) {
                direction -= Vector3.right;
            }
            if(Input.GetKey(KeyCode.D)) {
                direction += Vector3.right;
            }*/
        if (direction.magnitude > 1) {
            direction.Normalize();
        }
        if (direction == Vector3.zero && !Input.GetKey(KeyCode.LeftShift)) {
            inputs[0] = null;
        } else {
            inputs[0] = direction;//.normalized;
        }
#endif
#if !UNITY_EDITOR
		if(inputs[0] != null) {
			direction = inputs[0].Value - touchPosition;
			if(direction.magnitude < deadZoneSize) {
				direction = Vector3.zero;
			} else if (direction.magnitude > maxZoneSize) {
				direction = new Vector3(direction.x, 0, direction.y).normalized;
			} else {
				direction = (direction.magnitude / maxZoneSize) * new Vector3(direction.x, 0, direction.y).normalized;
			}
			inputs[0] = direction;
		}
#endif
        if (inputs[0] != null) {
            inputs[0] = (inputs[0].Value.x * Camera.main.transform.right) + (inputs[0].Value.z * Camera.main.transform.up);
            if(Vector3.Project(cM.transform.up, Camera.main.transform.forward) != cM.transform.up) {
                Vector3 rightComp = Vector3.Project(inputs[0].Value, Vector3.Cross(cM.transform.up, Camera.main.transform.forward));
                Vector3 upComp = inputs[0].Value - rightComp;
                Vector3 upUnit = Vector3.Cross(Camera.main.transform.forward, Vector3.Cross(cM.transform.up, Camera.main.transform.forward)).normalized;// * Vector3.Dot(Vector3.Project(Camera.main.transform.up, cM.transform.up).normalized, cM.transform.up);
                Vector3 towardsCM = Vector3.Cross(Vector3.Cross(cM.transform.up, Camera.main.transform.forward), cM.transform.up).normalized;
                if (Vector3.Project(upUnit + Camera.main.transform.forward, towardsCM) == Vector3.zero) {
                    inputs[0] = (Vector3.Project(Camera.main.transform.forward, towardsCM).normalized * upComp.magnitude * Vector3.Dot(upUnit, upComp.normalized)) + rightComp;
                } else {
                    inputs[0] = (Vector3.Project(upUnit + Camera.main.transform.forward, towardsCM).normalized * upComp.magnitude * Vector3.Dot(upUnit, upComp.normalized)) + rightComp;
                }
            }
        }
	}

	static ButtonFunktion BFInput () {
		if(inputs[0] != null)
			DeleteMe.input3 = inputs [0].Value;
		for (int i = inputs.Length - 1; i > -1; i--) {
			if(inputs[i] != null) {
				for (int e = 0; e < ButtonFunktion.rects.Length; e++) {
					if (RectTransformUtility.RectangleContainsScreenPoint (ButtonFunktion.rects[e], inputs[i].Value, Camera.current)) {
						return ButtonFunktion.buttons[e];
					}
				}
			}
		}
		return null;
	}

	public static void Manager() {
		#if UNITY_EDITOR
		GetMousePoint();
		#endif
		#if !UNITY_EDITOR
		GetTouchPoints();
		#endif
		bFinput = BFInput ();
		UIManager.SetInput (bFinput);
		if (!bFinput && Time.timeScale != 0) {
			Direction ();
		} else {
			inputs[0] = Vector3.zero;
			inputs[1] = null;
		}
		if(Time.timeScale != 0) {
			cM.SetInputs (inputs);
            if(Input.GetKeyDown(KeyCode.R)) {
                cM.Resurect();
            }
		}
	}

	void Awake () {
		cM = publicCM;
		#if !UNITY_EDITOR
		deadZoneSize = publicDeadZoneSize;
		maxZoneSize = publicMaxZoneSize;
		#endif
	}

	// Use this for initialization
	void Start () {
        if (!cM) {
            for (int i = 0; i < CharacterMover.cMList.Count; i++) {
                if (CharacterMover.cMList[i].myCharacterType == CharacterMover.CharacterType.Player) {
                    cM = CharacterMover.cMList[i];
                }
            }
        }
    }
	
	// Update is called once per frame
	void Update () {

	}
}
