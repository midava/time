﻿using UnityEngine;
using System.Collections;

public class ChangeCameraRotation : MonoBehaviour {

    public Vector3 rotation;

    public void OnTriggerEnter(Collider other) {
		if (other.CompareTag ("Player")) {
			GameObject.Find ("CamRot").GetComponent<CameraRotationController> ().ChangeRotation (Quaternion.Euler (rotation));
		}
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
