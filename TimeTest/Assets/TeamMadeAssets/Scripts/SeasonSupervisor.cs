﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SeasonSupervisor : MonoBehaviour {

    public static List<SeasonSupervisor> SSList = new List<SeasonSupervisor>();

    public Material summerMaterial;
    public Material winterMaterial;

    MeshRenderer myMesh;

    public void ChangeMaterial() {
        if (TimeManager.summer) {
            if (myMesh.material != summerMaterial) {
                Destroy(myMesh.material);
                myMesh.material = summerMaterial;
            }
        } else {
            if (myMesh.material != winterMaterial) {
                Destroy(myMesh.material);
                myMesh.material = winterMaterial;
            }
        }
    }

    void Awake() {
        SSList.Add(this);
        myMesh = gameObject.GetComponent<MeshRenderer>();
        if (!myMesh)
            Debug.LogError("Object must have MeshRenderer");
    }

    // Use this for initialization
    void Start() {
        ChangeMaterial();
    }

    // Update is called once per frame
    void Update() {
        
    }
}
