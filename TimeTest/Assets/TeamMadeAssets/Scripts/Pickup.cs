﻿using UnityEngine;
using System.Collections;

public class Pickup : MonoBehaviour {

    public float healAmount = 5;
    public float manaAmount = 5;

    public GameObject prefab;

    void OnTriggerEnter(Collider c) {
        if (c.GetComponent<CharacterMover>().myCharacterType != CharacterMover.CharacterType.Player)
            return;
        if(healAmount != 0) {
            c.GetComponent<Health>().Heal(healAmount);
        }
        if (manaAmount != 0) {
            c.SendMessage("AddMana", 50);
        }
        GameObject Me = GameObject.Instantiate(prefab);
        Me.transform.position = transform.position;
        Destroy(Me, 10);
        Destroy (gameObject);
	}
    // Use this for initialization
    void Start () {
	
	}
	
	
	void Update ()
	{

	}
}

