﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public interface UpdateSyncronizer {
      void EarlySyncedUpdate();
      void SyncedUpdate();
}

public class UpdateManger : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}

    void funktionLoop (List<UpdateSyncronizer> u) {
        for (int i = 0; i < AIBase.AIList.Count; i++) {
            if (u[i] != null) {
                u[i].SyncedUpdate();
            } else {
                u.RemoveAt(i);
            }
        }
    }
	
	// Update is called once per frame
	void Update () {
		InputManager.Manager();
		UIManager.Manager();
		for (int i = 0; i < CharacterMover.cMList.Count; i ++) {
			CharacterMover.cMList[i].SetTimeScale ();
		}
		if (Time.timeScale != 0) {
            funktionLoop(AIBase.AIList);
            for (int i = 0; i < CharacterMover.cMList.Count; i ++) {
				CharacterMover.cMList [i].MoveCharacter ();
			}
		}
	}
}
