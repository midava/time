﻿using UnityEngine;
using System.Collections;

public class CameraRotationController : MonoBehaviour {

    public float axisAngleSpeed = 60;

    Quaternion originalRotation;
    Quaternion currentRotation;

    public void ResetRotation() {
        currentRotation = originalRotation;
    }

    public void ChangeRotation(Quaternion q) {
        currentRotation = q;
    }

	// Use this for initialization
	void Start () {
        originalRotation = transform.rotation;
        currentRotation = originalRotation;
	}
	
	// Update is called once per frame
	void Update () {
	    if(Time.timeScale != 0 && transform.rotation != currentRotation) {
            if ((currentRotation.eulerAngles.y - transform.eulerAngles.y > 0 && currentRotation.eulerAngles.y - transform.eulerAngles.y < 180) || currentRotation.eulerAngles.y - transform.eulerAngles.y < -180) {
                if (currentRotation.eulerAngles.y - transform.eulerAngles.y > axisAngleSpeed * Time.unscaledDeltaTime || (currentRotation.eulerAngles.y - transform.eulerAngles.y + 360 > axisAngleSpeed * Time.unscaledDeltaTime && currentRotation.eulerAngles.y - transform.eulerAngles.y + 360 < 360)) {
                    transform.eulerAngles += Vector3.up * axisAngleSpeed * Time.unscaledDeltaTime;
                } else {
                    transform.eulerAngles += (currentRotation.eulerAngles.y - transform.eulerAngles.y) * Vector3.up;
                }
            } else if (currentRotation.eulerAngles.y - transform.eulerAngles.y != 0) {
                if (currentRotation.eulerAngles.y - transform.eulerAngles.y < -axisAngleSpeed * Time.unscaledDeltaTime || (currentRotation.eulerAngles.y - transform.eulerAngles.y - 360 < -axisAngleSpeed * Time.unscaledDeltaTime && currentRotation.eulerAngles.y - transform.eulerAngles.y - 360 > -360)) {
                    transform.eulerAngles += Vector3.down * axisAngleSpeed * Time.unscaledDeltaTime;
                } else {
                    transform.eulerAngles += (currentRotation.eulerAngles.y - transform.eulerAngles.y) * Vector3.up;
                }
            } if ((currentRotation.eulerAngles.x - transform.eulerAngles.x > 0 && currentRotation.eulerAngles.x - transform.eulerAngles.x < 180) || currentRotation.eulerAngles.x - transform.eulerAngles.x < -180) {
                if(currentRotation.eulerAngles.x - transform.eulerAngles.x > axisAngleSpeed * Time.unscaledDeltaTime || (currentRotation.eulerAngles.x - transform.eulerAngles.x + 360 > axisAngleSpeed * Time.unscaledDeltaTime && currentRotation.eulerAngles.x - transform.eulerAngles.x + 360 < 360)) {
                    transform.eulerAngles += Vector3.right * axisAngleSpeed * Time.unscaledDeltaTime;
                } else {
                    transform.eulerAngles += (currentRotation.eulerAngles.x - transform.eulerAngles.x) * Vector3.right;
                }
            } else if(currentRotation.eulerAngles.x - transform.eulerAngles.x != 0) {
                if (currentRotation.eulerAngles.x - transform.eulerAngles.x < -axisAngleSpeed * Time.unscaledDeltaTime || (currentRotation.eulerAngles.x - transform.eulerAngles.x - 360 < -axisAngleSpeed * Time.unscaledDeltaTime && currentRotation.eulerAngles.x - transform.eulerAngles.x - 360 > -360)) {
                    transform.eulerAngles += Vector3.left * axisAngleSpeed * Time.unscaledDeltaTime;
                } else {
                    transform.eulerAngles += (currentRotation.eulerAngles.x - transform.eulerAngles.x) * Vector3.right;
                }
            }
            if ((currentRotation.eulerAngles.z - transform.eulerAngles.z > 0 && currentRotation.eulerAngles.z - transform.eulerAngles.z < 180) || currentRotation.eulerAngles.z - transform.eulerAngles.z < -180) {
                if (currentRotation.eulerAngles.z - transform.eulerAngles.z > axisAngleSpeed * Time.unscaledDeltaTime || (currentRotation.eulerAngles.z - transform.eulerAngles.z + 360 > axisAngleSpeed * Time.unscaledDeltaTime && currentRotation.eulerAngles.z - transform.eulerAngles.z + 360 < 360)) {
                    transform.eulerAngles += Vector3.forward * axisAngleSpeed * Time.unscaledDeltaTime;
                } else {
                    transform.eulerAngles += (currentRotation.eulerAngles.z - transform.eulerAngles.z) * Vector3.forward;
                }
            } else if (currentRotation.eulerAngles.z - transform.eulerAngles.z != 0) {
                if (currentRotation.eulerAngles.z - transform.eulerAngles.z < -axisAngleSpeed * Time.unscaledDeltaTime || (currentRotation.eulerAngles.z - transform.eulerAngles.z - 360 < -axisAngleSpeed * Time.unscaledDeltaTime && currentRotation.eulerAngles.z - transform.eulerAngles.z - 360 > -360)) {
                    transform.eulerAngles += Vector3.back * axisAngleSpeed * Time.unscaledDeltaTime;
                } else {
                    transform.eulerAngles += (currentRotation.eulerAngles.z - transform.eulerAngles.z) * Vector3.forward;
                }
            }
        }
	}
}
