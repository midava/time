﻿using UnityEngine;
using System.Collections;

public class PlayerDiedWhatTDo : MonoBehaviour {

    public CharacterMover player;
    public float resetTime = 5;
    float timer = 0;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    if(player.IsDead()) {
            timer += Time.unscaledDeltaTime;
            if (timer > resetTime) {
                AIBase.AIList.Clear();
                CharacterMover.cMList.Clear();
                SeasonSupervisor.SSList.Clear();
                Application.LoadLevel(Application.loadedLevel);
            }
        } else {
            timer = 0;
        }
	}
}
