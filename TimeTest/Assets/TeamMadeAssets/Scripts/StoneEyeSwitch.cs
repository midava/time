﻿using UnityEngine;
using System.Collections;

public class StoneEyeSwitch : MonoBehaviour {

    public GameObject[] objects;

    public bool initialSwitchState = true;

    bool switchOn = true;

    public void TurnSwich () {
        switchOn = !switchOn;
        foreach(GameObject g in objects) {
            if (g.activeSelf != switchOn) {
                g.SetActive(switchOn);
            }
        }
    }

    void Awake () {
        switchOn = initialSwitchState;
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
