﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WeaponTrigger : MonoBehaviour {

    CharacterMover CM;
    List<Collider> hitColliders = new List<Collider>();

    void OnEnable () {
        hitColliders.Clear();
    }

    void OnTriggerEnter (Collider c) {
        if(CM.weapon && !hitColliders.Contains(c) && c.gameObject != CM.gameObject) {
            c.GetComponent<Health>().Damage(CM.Damage());
            hitColliders.Add(c);
        }
    }

    void Awake() {
        CM = gameObject.GetComponentInParent<CharacterMover>();
    }

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
