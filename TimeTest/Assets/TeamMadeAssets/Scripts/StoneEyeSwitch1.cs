﻿using UnityEngine;
using System.Collections;

public class StoneEyeSwitch1 : MonoBehaviour {

    public GameObject[] objects;

    

	public void TurnSwich () {
	
        foreach(GameObject g in objects) {
				g.GetComponent<Animator> ().SetBool ("Play", true);
            }
        }

	// Use this for initialization
	void Start () {
		foreach (GameObject g in objects) {
			g.GetComponent<Animator> ().SetBool ("Play", false);
            return;
	}
	
	}
}