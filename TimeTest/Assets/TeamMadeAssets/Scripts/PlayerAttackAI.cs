﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerAttackAI : AIBase {

    CharacterMover CM;
    Collider target;
    List<Collider> targets = new List<Collider>();

    public override void GiveInputs() {
        if (target != null && !target.enabled) {
            targets.Remove(target);
            target = null;
        }
        if (target == null && targets.Count > 0) {
            for (int i = 0; i < targets.Count; i++) {
                if (targets[i] == null) {
                    targets.RemoveAt(i);
                } else if (target == null) {
                    target = targets[i];
                }
            }
        }
        if (target != null) {
            CM.SetInputs((target.transform.position - CM.transform.position - ((target.transform.position - CM.transform.position).y * Vector3.up)), true);
        } else {
            CM.SetInputs(Vector3.zero, false);
        }
    }

    void OnTriggerEnter(Collider c) {
        if (c.gameObject.GetComponent<CharacterMover>().myCharacterType == CharacterMover.CharacterType.Enemy) {
            targets.Add(c);
        }
    }

    void OnTriggerExit(Collider c) {
        if (c.gameObject.GetComponent<CharacterMover>().myCharacterType == CharacterMover.CharacterType.Enemy) {
            targets.Remove(c);
            if (target == c) {
                target = null;
            }
        }
    }

    void Awake() {
        CM = gameObject.GetComponentInParent<CharacterMover>();
        AIList.Add(this);
    }

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }
}
