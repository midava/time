﻿using UnityEngine;
using System.Collections;

public class UIManager : MonoBehaviour {
	
	public RectTransform publicMainButton;
	public RectTransform publicButtonRoot;
	
	public float publicMenuMaxSpeed = 150;
	public float publicMenuAcceleration = 500;

	static RectTransform mainButton;
	static RectTransform buttonRoot;

	static float menuMaxSpeed = 150;
	static float menuAcceleration = 500;

	static Vector3 positionUp;
	static Vector3 positionDown;

	static Vector3 menuVelocity;

	static ButtonFunktion input; 

	static bool inputAllowed = true;
	static bool menuOpen = false;

	public static void SetInput (ButtonFunktion i) {
		input = i;
	}

	public static void CloseMenu () {
		if (menuOpen) {
			menuVelocity = Vector3.up * menuMaxSpeed;
			inputAllowed = false;
		}
	}

	public static void ToggleMenu () {
		inputAllowed = false;
		if (menuOpen) {
			menuVelocity = Vector3.up * menuMaxSpeed;
		} else {
			menuVelocity = Vector3.down * menuMaxSpeed;
			Time.timeScale = 0;
		}
	}

	public static void Manager () {
		if (inputAllowed && input) {
			input.ButtonsFunktion();
		}
		if (!inputAllowed) {
			if(buttonRoot.position.y < positionDown.y && menuVelocity.y < menuMaxSpeed) {
				menuVelocity += Time.unscaledDeltaTime * menuAcceleration * Vector3.up;
			} else if (buttonRoot.position.y >= positionDown.y && menuVelocity.y > 0 && !menuOpen) {
				menuVelocity = Vector3.zero;
				buttonRoot.position += (positionDown.y - buttonRoot.position.y) * Vector3.up;
				menuOpen = true;
				inputAllowed = true;
			} else if (buttonRoot.position.y >= positionUp.y && menuVelocity.y > 0) {
				buttonRoot.position += (positionUp.y - buttonRoot.position.y) * Vector3.up;
				menuOpen = false;
				inputAllowed = true;
				menuVelocity = Vector3.zero;
				Time.timeScale = TimeManager.timeScale;
			}
			buttonRoot.position += menuVelocity * Time.unscaledDeltaTime;
		}
	}

	void Awake () {
		mainButton = publicMainButton;
		buttonRoot = publicButtonRoot;
		menuMaxSpeed = publicMenuMaxSpeed;
		menuAcceleration = publicMenuAcceleration;
	}

	// Use this for initialization
	void Start () {
		positionUp = buttonRoot.position;
		positionDown = positionUp + ((mainButton.position.y - positionUp.y) * Vector3.up);
	}
	
	// Update is called once per frame
	void Update () {

	}
}
