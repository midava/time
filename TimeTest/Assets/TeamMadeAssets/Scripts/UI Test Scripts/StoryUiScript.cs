﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class StoryUiScript : MonoBehaviour {

	public float time = 3.0f;

	public GameObject CurrentImage;

	public GameObject[] objects;
	public GameObject[] Next;

	int ImageNumber = -1;

	float a = 1.0f;
	float timeLeft = 10f;

	Image im;

    public void Skip() {
        foreach (GameObject g in objects)
        {
            g.SetActive(true);
        }
        CurrentImage.SetActive(false);
        Destroy(transform.parent.gameObject);
    }

	void FadeOut(){

		a = Mathf.Max (a-Time.deltaTime/2,0);
		im = CurrentImage.GetComponent<Image> ();
		im.CrossFadeAlpha (0,0.5f,false);
		//CurrentImage.SetActive (false);

	}

	void NextImage(){
		ImageNumber = ImageNumber + 1;
		FadeOut ();
		Next [ImageNumber].SetActive (true);
		CurrentImage = Next[ImageNumber];
		timeLeft = time;
	}

	// Use this for initialization
	void Start () {
		timeLeft = time;
	}
	
	// Update is called once per frame
	void Update () {
		if (ImageNumber <= 5) {
			timeLeft -= Time.deltaTime;
			if (timeLeft < 0) {
				NextImage ();
			}
		} else {
            Skip();
		}
		}
	
	}
