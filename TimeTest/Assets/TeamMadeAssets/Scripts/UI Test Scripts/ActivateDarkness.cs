﻿using UnityEngine;
using System.Collections;

public class ActivateDarkness : MonoBehaviour {

    public GameObject Darkness;

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Darkness.GetComponent<BlackScreen>().FadeIn();
        }
    }

            // Use this for initialization
            void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
