﻿using UnityEngine;
using System.Collections;

public class ManapoolScript : MonoBehaviour {

	void OnTriggerStay (Collider other) {
		if (other.CompareTag ("Player")) {
            other.SendMessage("AddMana", 0.2f);
		}
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
