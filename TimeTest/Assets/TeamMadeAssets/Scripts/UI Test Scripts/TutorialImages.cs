﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class TutorialImages : MonoBehaviour {
	Image ThisImage;
	float a = 1.0f;
	Image im;
	float timeLeft = 0.2f;
	bool Death = false;


	public void FadeOut(){
		a = Mathf.Max (a-Time.deltaTime/2,0);
		im = ThisImage.GetComponent<Image> ();
		im.CrossFadeAlpha (0,0.2f,false);
		Death = true;

	}
	// Use this for initialization
	void Start () {
	ThisImage = GetComponent<Image> ();
	a = Mathf.Max (a-Time.deltaTime/2,0);
	im = ThisImage.GetComponent<Image> ();
	im.CrossFadeAlpha (0,0f,false);
	im.CrossFadeAlpha (1,0.8f,false);
	}
	
	// Update is called once per frame
	void Update () {
	if (Death == true){
	timeLeft -= Time.deltaTime;
	if (timeLeft < 0) {
		gameObject.SetActive (false);
	}
	}
	}
}
