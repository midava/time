﻿using UnityEngine;
using System.Collections;

public class ColliderActivator : MonoBehaviour {

	public GameObject ActivateObject;

	void OnTriggerEnter (Collider other) {
		if (other.CompareTag ("Player")) {
			ActivateObject.SetActive (true);
			Destroy (gameObject);
		}
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
