﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class BlackScreen : MonoBehaviour {

    public GameObject ThisImage;
    float a = 1.0f;
    Image im;
    public float timeLeft = 0.6f;
    public bool timer = false;

    bool setfalse = false;

    bool timeChange = false;

    public bool FirstTime = false;

    public void Change() {
        timeChange = true;
        FadeIn();
    }

    public void FadeIn()
    {
        timer = true;
        ThisImage.SetActive(true);
        setfalse = false;
        a = Mathf.Max(a - Time.deltaTime / 2, 0);
        im = ThisImage.GetComponent<Image>();
        im.CrossFadeAlpha(1, 0.5f, true);
        
    }

    public void FadeOut()
    {
        timer = true;
        setfalse = true;
        FirstTime = false;
        a = Mathf.Max(a - Time.deltaTime / 2, 0);
        im = ThisImage.GetComponent<Image>();
        im.CrossFadeAlpha(0, 0.5f, false);
    }
        // Use this for initialization
        void Start () {
        if (FirstTime == true)
        {
            FadeOut();
        }
    }
	
	// Update is called once per frame
	void Update () {
        if (timer == true) {
            timeLeft -= Time.deltaTime;
        }

        if (timeLeft < 0 && setfalse == true){
            ThisImage.SetActive(false);
            timeLeft = 0.6f;
            timer = false;
        }
        if (timeLeft < 0 && setfalse == false)
        {
            FadeOut();
            timeLeft = 0.6f;
            timer = false;
            if (timeChange == true) {
                FadeOut();
            }
        }
    }
}
