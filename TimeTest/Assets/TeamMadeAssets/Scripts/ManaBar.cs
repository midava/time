﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ManaBar : MonoBehaviour {

    public CharacterMover mana;
    Image manaBar;

    void Awake() {
        manaBar = gameObject.GetComponent<Image>();
    }

    // Use this for initialization
    void Start() {
        if (!mana) {
            for (int i = 0; i < CharacterMover.cMList.Count; i++) {
                if (CharacterMover.cMList[i].myCharacterType == CharacterMover.CharacterType.Player) {
                    mana = CharacterMover.cMList[i];
                }
            }
        }
    }

    // Update is called once per frame
    void Update() {
        manaBar.fillAmount = mana.GetMana() / mana.maxMana;
    }
}
