﻿using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour {

	public float maxHealth = 10;
    float health;

    CharacterMover CM;

    public float GetHealth () {
        return health;
    }

	public void Death(){
		health = 0;
	}

	public void Damage (float damage) {
        if (!CM.UnDamagable()) {
            health -= damage;
            if (health <= 0) {
                CM.Die();
            } else {
                CM.SetDamaged();
                if(CM.myCharacterType == CharacterMover.CharacterType.Player) {
                    Handheld.Vibrate();
                }
            }
        }
	}

    public void Heal (float heal) {
        health += heal;
        if(health > maxHealth) {
            health = maxHealth;
        }
    }

    void Awake()
    {
        CM = gameObject.GetComponent<CharacterMover>();
        health = maxHealth;
    }

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (health <= 0) {
			CM.Die ();
		}
	}
}
