﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AI2 : AIBase {

    public float agroRange = 10;
    public float attackRange = 2;
    public float attackDelay = 1;
    public float attackDuration = 5;

    public GameObject target;

	CharacterMover CM;
	Vector3 anchor;

	Vector3 direction;
	Vector3?[] inputs = new Vector3?[3];
    bool attack = false;
    bool unAgrod = true;

	float timer = 0;

	public override void GiveInputs () {
        attack = false;
        inputs[0] = Vector3.zero;
        inputs[2] = Vector3.zero;
        if (!target || ((target.transform.position - anchor).magnitude > agroRange && unAgrod)) {
            timer = 0;
            if ((transform.position - anchor).magnitude > 1) {
                direction = anchor - transform.position;
                direction -= Vector3.up * direction.y;
                inputs[0] = direction.normalized;
            }
        } else {
            unAgrod = false;
            if ((target.transform.position - transform.position).magnitude > attackRange) {
                timer = 0;
                direction = target.transform.position - transform.position;
                direction -= Vector3.up * direction.y;
                inputs[0] = direction.normalized;
            } else {
                direction = target.transform.position - transform.position;
                direction -= Vector3.up * direction.y;
                inputs[2] = direction.normalized;
                timer += (1 / CM.GiveTimeScale()) * Time.deltaTime;
                if(timer > attackDelay + attackDuration) {
                    timer = 0;
                } else if(timer > attackDelay) {
                    attack = true;
                }
            }
        }
        CM.SetInputs(inputs, attack);
    }

	void Awake () {
		CM = gameObject.GetComponent<CharacterMover> ();
		AIList.Add (this);
	}

	// Use this for initialization
	void Start () {
		CM = gameObject.GetComponent<CharacterMover> ();
		anchor = transform.position;
        if(!target) {
            for(int i = 0; i < CharacterMover.cMList.Count; i ++) {
                if(CharacterMover.cMList[i].myCharacterType == CharacterMover.CharacterType.Player) {
                    target = CharacterMover.cMList[i].gameObject;
                }
            }
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnDestroy () {
        AIList.Remove(this);
    }
}
