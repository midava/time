﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AIBase : MonoBehaviour , UpdateSyncronizer {

    public static List<UpdateSyncronizer> AIList = new List<UpdateSyncronizer>();

    public virtual void GiveInputs() {

    }

    public void EarlySyncedUpdate() {

    }

    public void SyncedUpdate () {
        GiveInputs();
    }
}
