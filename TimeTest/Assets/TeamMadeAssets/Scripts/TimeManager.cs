﻿using UnityEngine;
using System.Collections;

public class TimeManager : MonoBehaviour {

	public GameObject publicSummerRoot;
	public GameObject publicWinterRoot;

    public Texture2D publicSummerOverLay;
    public Texture2D publicWinterOverLay;

	public static bool summer = true;
	public static float timeScale = 1;

	static GameObject summerRoot;
	static GameObject winterRoot;

    static Texture2D summerOverLay;
    static Texture2D winterOverLay;

    static CharacterMover player;

	public static void ChangeSeason () {
		summer = !summer;
		if(summerRoot.activeSelf != summer) {
			summerRoot.SetActive (summer);
		}
		if(winterRoot.activeSelf == summer) {
			winterRoot.SetActive (!summer);
		}
        for(int i = 0; i < SeasonSupervisor.SSList.Count; i++) {
            SeasonSupervisor.SSList[i].ChangeMaterial();
        }
        if (player)
            player.ChangePersona();
        if(summer) {
            Camera.main.GetComponent<UnityStandardAssets.ImageEffects.ScreenOverlay>().texture = summerOverLay;
        } else {
            Camera.main.GetComponent<UnityStandardAssets.ImageEffects.ScreenOverlay>().texture = winterOverLay;
        }
	}

	void Awake () {
        if (!publicSummerRoot || !publicWinterRoot)
            Debug.LogError("You must set refernece to both summer- and winterroot");
		summerRoot = publicSummerRoot;
		winterRoot = publicWinterRoot;
        if (!publicSummerOverLay|| !publicWinterOverLay)
            Debug.LogError("You must set refernece to both summer- and winterOverlay");
        summerOverLay = publicSummerOverLay;
        winterOverLay = publicWinterOverLay;
    }

	// Use this for initialization
	void Start () {
		winterRoot.SetActive (!summer);
		summerRoot.SetActive (summer);
		timeScale = Time.timeScale;
        if (!player) {
            for (int i = 0; i < CharacterMover.cMList.Count; i++) {
                if (CharacterMover.cMList[i].myCharacterType == CharacterMover.CharacterType.Player) {
                    player = CharacterMover.cMList[i];
                }
            }
        }
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
