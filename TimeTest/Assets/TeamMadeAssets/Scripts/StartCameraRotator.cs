﻿using UnityEngine;
using System.Collections;

public class StartCameraRotator : MonoBehaviour {

    public float anglesPerSecond = 30;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(0, Time.deltaTime * anglesPerSecond, 0);
	}
}
